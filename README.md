# adone

This is a hand-and-mouse trace of a font originally designed to
be used at a single small point-size for ads.

![An alphabet](adone-alpha.png)


## Design Notes

With UPM approx 1000 (to clarify, currently the UPM is 1000, but
the design has very short descenders, so this may need to
change).

Being a trace, these are derived from the starting material.

- ascender stem height 782
- x-height set to 566 based on the **x**
- descender is -125 (**p** **q**); -126 (**g** **j**); should unify
- bowl overshoot -23 (on **b** **c** **d**)
- **v** overshoot -15 (on **v** **w**)
- stem width 134 (varies a little bit)
- **f** reaches 805 (stem height + 23 of overshoot)
- top of **i** is 783 (should adjust to match ascender stem height)

- d is a reflection of b!
- q is a reflection of p!
- u is a reflection of n! (very nearly, inside bowl adjusted)


## names

- Adone, pronounced Ad One (working title)
- Ad Free, when it becomes SIL?
- Adoubt
- Adeja

# END
